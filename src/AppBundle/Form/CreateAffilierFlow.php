<?php
/**
 * Created by PhpStorm.
 * User: tritux
 * Date: 04/06/18
 * Time: 09:03
 */

namespace AppBundle\Form;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Craue\FormFlowBundle\Event\GetStepsEvent;
use Craue\FormFlowBundle\Event\PostBindFlowEvent;
use Craue\FormFlowBundle\Event\PostBindRequestEvent;
use Craue\FormFlowBundle\Event\PostBindSavedDataEvent;
use Craue\FormFlowBundle\Event\PostValidateEvent;
use Craue\FormFlowBundle\Event\PreBindEvent;
use Craue\FormFlowBundle\Form\FormFlowEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CreateAffilierFlow extends FormFlow
{

    //protected $revalidatePreviousSteps = true;
    //protected $dynamicStepNavigationInstanceParameter = 'instance';
    protected $allowDynamicStepNavigation = true;
    protected function loadStepsConfig() {
		return array(
			array(
				'label' => 'Donnee personnel',
				'form_type' => 'AppBundle\Form\CreateAffilierForm',
                'form_options' => array(
                    'validation_groups' => array('Default'),
                ),

			),
			array(
				'label' => 'Donnee complementaire ',
				'form_type' => 'AppBundle\Form\CreateAffilierForm',
				'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
					return $estimatedCurrentStepNumber > 1 && !$flow->getFormData()->getName();
				},
			),
			array(
				'label' => 'confirmation',
			),
		);
	}

}