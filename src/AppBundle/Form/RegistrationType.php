<?php
/**
 * Created by PhpStorm.
 * User: tritux
 * Date: 16/05/18
 * Time: 11:16
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')

            ->add('Groups',EntityType::class, array(
                'class' => 'AppBundle:Group',
                'label' => 'choose role',
                'multiple' => true,
                'expanded' => true
            ))
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}