<?php
/**
 * Created by PhpStorm.
 * User: tritux
 * Date: 04/06/18
 * Time: 09:07
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class CreateAffilierForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        switch ($options['flow_step']) {
            case 1:
                $validValues = array(2, 4);
                $builder
                   /* ->add('numberOfWheels', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                    'choices' => array_combine($validValues, $validValues),
                    'placeholder' => '',
                ));*/
                   ->add('name')
                   ->add('email')
                    ->add('dateNaissance', DateType::class, array(
                        'placeholder' => '--',
                    ))
                    ->add('password' , HiddenType::class, array(
                        'required'   => false,
                        'empty_data' => 'test',
                    ))
                   ->add('cin');
                break;
            case 2:
                $builder
                // This form type is not defined in the example.
                /*->add('engine', 'AppBundle\Form\Type\VehicleEngineType', array(
                    'placeholder' => '',
                ));*/
                ->add('username')
                ->add('etat' , HiddenType::class, array(
                'required'   => false,
                'empty_data' => '0',
            ));
                break;
        }
    }

    public function getBlockPrefix() {
        return 'createAffilier';
    }

}