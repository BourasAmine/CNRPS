<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        /*return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);*/

        return $this->render('homePage.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route ("/organisation/" , name="organisation_page")
     */

    public function organisationPageAction(){

        return $this->render('organisation.html.twig');

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route ("/affilier/" , name="affilier_page")
     */

    public function AffilerPageAction(){

        return $this->render('affilier.html.twig');

    }
}
