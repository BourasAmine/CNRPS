<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carriere
 *
 * @ORM\Table(name="carriere")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CarriereRepository")
 */
class Carriere
{

    /**
     * One Carriere to One Affilier.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="carriere")
     * @ORM\JoinColumn(name="affiler_id", referencedColumnName="id")
     */
    private $affilier;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_debut", type="datetime", nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_fin", type="datetime", nullable=true)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="Grade", type="string", length=255, nullable=true)
     */
    private $grade;

    /**
     * @var string
     *
     * @ORM\Column(name="Fonction", type="string", length=255, nullable=true)
     */
    private $fonction;

    /**
     * @var string
     *
     * @ORM\Column(name="NatureDeService", type="string", length=255, nullable=true)
     */
    private $natureDeService;

    /**
     * @var string
     *
     * @ORM\Column(name="CotisationRegler", type="string", length=255, nullable=true)
     */
    private $cotisationRegler;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Carriere
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Carriere
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set grade
     *
     * @param string $grade
     *
     * @return Carriere
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     *
     * @return Carriere
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set natureDeService
     *
     * @param string $natureDeService
     *
     * @return Carriere
     */
    public function setNatureDeService($natureDeService)
    {
        $this->natureDeService = $natureDeService;

        return $this;
    }

    /**
     * Get natureDeService
     *
     * @return string
     */
    public function getNatureDeService()
    {
        return $this->natureDeService;
    }

    /**
     * Set cotisationRegler
     *
     * @param string $cotisationRegler
     *
     * @return Carriere
     */
    public function setCotisationRegler($cotisationRegler)
    {
        $this->cotisationRegler = $cotisationRegler;

        return $this;
    }

    /**
     * Get cotisationRegler
     *
     * @return string
     */
    public function getCotisationRegler()
    {
        return $this->cotisationRegler;
    }

    /**
     * @return mixed
     */
    public function getAffilier()
    {
        return $this->affilier;
    }

    /**
     * @param mixed $affilier
     */
    public function setAffilier($affilier)
    {
        $this->affilier = $affilier;
    }


}

